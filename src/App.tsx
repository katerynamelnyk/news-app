import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import HomePage from './components/Main/HomePage';
import NewsPage from './components/Main/NewsPage';
import Layout from "./components/Layout";

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <HomePage />,
      },
      {
        path: '/news/:name',
        element: <NewsPage />,
      },
    ]
  },
]);

const App = () => {
  return (
    <RouterProvider router={router} />
  )
}

export default App;

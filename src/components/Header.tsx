import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { viewActions } from '../store';

import classes from './Header.module.css';

const Header = () => {
    const dispatch = useDispatch();
    
    const onChangeViewHandler = () => {
        dispatch(viewActions.changeTypeOfView());
    };

    return (
        <header className={classes.container}>
            <Link to="/" className={classes.link}>
                getNews
            </Link>
            <div className={classes['buttons-wrapper']}>
                <button onClick={onChangeViewHandler}>
                    Change Type Of View
                </button>
            </div>
        </header>
    );
}

export default Header;

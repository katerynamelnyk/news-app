import { NavLink } from 'react-router-dom';

import classes from './MenuItem.module.css';

type Props = {
    apiKey: string,
    name: string,
};

const MenuItem = (props: Props) => {
    return (
        <NavLink
            to={`/news/${props.apiKey}`}
            className={({ isActive }) => isActive
                ? `${classes.container} ${classes.active}`
                : `${classes.container}`
            }
        >
            <div className={classes.name}>
                {props.name}
            </div>
        </NavLink>
    );
};

export default MenuItem;

import MenuItem from './MenuItem';

import classes from './Menu.module.css';

const categoriesData = [
    {
        name: 'World',
        apiKey: 'WORLD',
    },
    {
        name: 'National',
        apiKey: 'NATIONAL',
    },
    {
        name: 'Business',
        apiKey: 'BUSINESS',
    },
    {
        name: 'Technology',
        apiKey: 'TECHNOLOGY',
    },
    {
        name: 'Entertainment',
        apiKey: 'ENTERTAINMENT',
    },
    {
        name: 'Sports',
        apiKey: 'SPORTS',
    },
    {
        name: 'Science',
        apiKey: 'SCIENCE',
    },
    {
        name: 'Health',
        apiKey: 'HEALTH',
    },
];

const Menu = () => {
    const categories = categoriesData.map(item =>
        <MenuItem
            key={item.name}
            name={item.name}
            apiKey={item.apiKey}
        />);

    return (
        <div className={classes.container}>
            {categories}
        </div>
    );
}

export default Menu;

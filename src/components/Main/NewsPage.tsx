import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import ListItem from './NewsItem/ListItem';
import TileItem from "./NewsItem/TileItem";
import Item from "./NewsItem/Item";
import Popup from "../Popup";
import { NewsView, viewActions } from "../../store";

import classes from './NewsPage.module.css';

type News = {
    id: string,
    title?: string,
    date: string,
    source?: string,
    url?: string,
    urlToImage?: string,
    sourceLogo?: string,
};

const NewsPage = () => {
    const [isOpenNews, setIsOpenNews] = useState(false);
    const [openedNews, setOpenedNews] = useState<News>();
    const [isLoading, setIsLoading] = useState(false);

    const dispatch = useDispatch();

    const params = useParams();

    const typeOfView = useSelector((state: NewsView) => state.typeOfView);

    const [newsData, setNewsData] = useState<News[]>([]);

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            //account for RapidApi - melnikphoto@gmail.com
            const options = {
                method: 'GET',
                headers: {
                    'X-RapidAPI-Key': '0b427f8375msh89e52b3c21ccbc4p137502jsn582f0d5d3bcb',
                    'X-RapidAPI-Host': 'real-time-news-data.p.rapidapi.com'
                }
            };
            const response = await fetch(
                `https://real-time-news-data.p.rapidapi.com/topic-headlines?topic=${params.name}&country=US&lang=en`,
                options)

            if (response.ok) {
                const rawData = await response.json();

                const transformedData = rawData.data
                    .map((item: any) => ({
                        id: Math.random().toString(),
                        title: item.title,
                        date: item.published_datetime_utc,
                        source: item.source_url,
                        sourceLogo: item.source_logo_url,
                        url: item.link,
                        urlToImage: item.photo_url,
                    }))
                setNewsData(transformedData);
                dispatch(viewActions.setNewsLength(transformedData.length));
                setIsLoading(false);
            };
        })();
    }, [params.name, dispatch]);

    const onOpenNewsHandler = (news: News) => {
        setIsOpenNews(true);
        setOpenedNews(news);
    };
    const onCloseNewsHandler = () => {
        setIsOpenNews(false);
        setOpenedNews(undefined);
    };

    const newsElement = <Item
        title={openedNews?.title}
        url={openedNews?.url}
        source={openedNews?.source}
        urlToImage={openedNews?.urlToImage}
        sourceLogo={openedNews?.sourceLogo}
    />;

    const renderedNews = newsData.map(item =>
        typeOfView === "list"
            ?
            <ListItem
                key={item.id}
                title={item?.title}
                source={item?.source}
                date={item?.date}
                onOpenNews={() => onOpenNewsHandler(item)}
            />
            :
            <TileItem
                key={item.id}
                title={item?.title}
                source={item?.source}
                sourceLogo={item?.sourceLogo}
                date={item.date}
                urlToImage={item.urlToImage}
                onOpenNews={() => onOpenNewsHandler(item)}
            />
    );

    return (
        <div className={classes.container}>
            {isLoading && <div className={classes.load}>Loading...</div>}
            {typeOfView === "list" && !isLoading &&
                <div className={classes['flex-wrapper-list']}>
                    {renderedNews}
                </div>}
            {typeOfView === "tile" && !isLoading &&
                <div className={classes['flex-wrapper-tiles']}>
                    {renderedNews}
                </div>}
            {isOpenNews && <Popup
                onConfirm={onCloseNewsHandler}
                children={newsElement}
            />}
        </div>
    );
};

export default NewsPage;

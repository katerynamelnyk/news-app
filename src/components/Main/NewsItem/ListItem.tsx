import classes from './ListItem.module.css';

type Props = {
    title?: string,
    source?: string,
    date: string,
    onOpenNews: () => void,
};

const ListItem = (props: Props) => {
    const formattedDate = (new Date(props.date)).toLocaleString();

    return (
        <div className={classes.container} onClick={props.onOpenNews}>
            <h3>{props?.title}</h3>
            <p className={classes.source}>{props?.source}</p>
            <p>{formattedDate}</p>
        </div>
    );
};

export default ListItem;

import classes from './Item.module.css';

type Props = {
    title?: string,
    source?: string,
    urlToImage?: string,
    sourceLogo?: string,
    url?: string,
};

const Item = (props: Props) => {
    const imagePreview = props.urlToImage
        ? <img className={classes.picture} src={props?.urlToImage} alt={props.source} />
        : undefined;
    return (
        <div className={classes.container}>
            <h3>{props?.title}</h3>
            <img
                className={classes.logo}
                src={props?.sourceLogo}
                alt={props?.source}
            />
            <div className={classes['link-wrapper']}>
                <a href={props?.url}>{props?.url}</a>
            </div>
            {imagePreview}
        </div>
    );
};

export default Item;

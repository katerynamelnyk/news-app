import classes from './TileItem.module.css';

type Props = {
    title?: string,
    source?: string,
    date: string,
    urlToImage?: string,
    sourceLogo?: string,
    onOpenNews: () => void,
};

const TileItem = (props: Props) => {
    const formattedDate = (new Date(props.date)).toLocaleString();
    const imagePreview = props.urlToImage
        ? <img className={classes.picture} src={props?.urlToImage} alt={props.source} />
        : undefined;

    return (
        <div className={classes.container} onClick={props.onOpenNews} >
            <h3>{props.title}</h3>
            <img
                className={classes.logo}
                src={props.sourceLogo}
                alt={props.source}
            />
            <p className={classes.date}>{formattedDate}</p>
            {imagePreview}
        </div>
    );
};

export default TileItem;

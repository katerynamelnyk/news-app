import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

import { viewActions } from "../../store";

import classes from './HomePage.module.css';

const HomePage = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(viewActions.setNewsLength(0));
    }, [dispatch]);

    return (
        <div className={classes.container}>
            <h3>
                Find the latest breaking news and information on the top stories, weather, business, entertainment, politics, and more.
            </h3>
            <p>Just choose the category from the menu.</p>
        </div>
    );
};

export default HomePage;

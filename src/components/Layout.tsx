import { Outlet } from 'react-router-dom';

import Header from './Header';
import Menu from './Menu/Menu';
import Footer from './Footer';

import classes from './Layout.module.css';

const Layout = () => {
    return (
        <div className={classes.container}>
            <Header />
            <div className={classes['flex-wrapper']}>
                <Menu />
                <main>
                    <Outlet />
                </main>
            </div>
            <Footer />
        </div>
    )
}

export default Layout;
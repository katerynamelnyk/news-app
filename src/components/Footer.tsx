import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { NewsView } from '../store';

import classes from './Footer.module.css';

const getFormattedTime = () => {
    const hours = new Date().getHours().toString();
    const minutes = new Date().getMinutes().toString();
    const formattedHours = hours.length === 1
        ? `0${hours}` : `${hours}`;
    const formattedMinutes = minutes.length === 1
        ? `0${minutes}` : `${minutes}`;
    return `${formattedHours} : ${formattedMinutes}`;
};

const Footer = () => {
    const newsLength = useSelector((state: NewsView) => state.newsLength);
    const [currentTime, setCurrentTime] = useState(getFormattedTime());

    useEffect(() => {
        const timer = setInterval(() => {
            setCurrentTime(getFormattedTime());
        }, 1000);
        return () => clearInterval(timer);
    }, [currentTime]);

    const amountOfNews = newsLength > 0
        ? <p className={classes.length}>News: {newsLength}</p>
        : undefined;

    return (
        <footer className={classes.container}>
            <p className={classes.time}>{currentTime}</p>
            {amountOfNews}
        </footer>
    );
};

export default Footer;

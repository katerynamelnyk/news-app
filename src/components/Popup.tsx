import { ReactNode } from 'react';
import ReactDOM from 'react-dom';
import classes from './Popup.module.css';

type Props = {
    onConfirm: () => void,
    children?: ReactNode;
};

type BackdropProps = {
    onConfirm: () => void,
};

const Backdrop = (props: BackdropProps) => {
    return (
        <div className={classes.backdrop} onClick={props.onConfirm} />
    );
};


const PopupOverlay = (props: Props) => {
    return (
        <div className={classes.popup}>
            <div className={classes.content}>{props.children}
            </div>
            <button onClick={props.onConfirm} className={classes.button}>
                Close
            </button>
        </div>
    );
};


const Popup = (props: Props) => {
    return (
        <>
            {ReactDOM.createPortal(<Backdrop onConfirm={props.onConfirm} />,
                document.getElementById('backdrop-root') as HTMLBaseElement)}
            {ReactDOM.createPortal(<PopupOverlay onConfirm={props.onConfirm} children={props.children} />,
                document.getElementById('popup-root') as HTMLBaseElement)}
        </>
    );
};

export default Popup;

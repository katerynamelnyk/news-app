import { configureStore, createSlice } from "@reduxjs/toolkit";

export type NewsView = {
    typeOfView: 'list' | 'tile',
    newsLength: number,
}

const initialState: NewsView = {
    typeOfView: 'list',
    newsLength: 0,
};

const slice = createSlice({
    name: 'newsView',
    initialState,
    reducers: {
        changeTypeOfView: (state) => {
            state.typeOfView = state.typeOfView === 'list' ? 'tile' : 'list';
        },
        setNewsLength: (state, action) => {
            state.newsLength = action.payload;
        },
    },
})

export const store = configureStore({
    reducer: slice.reducer,
});

export const viewActions = slice.actions;
